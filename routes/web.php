<?php

use Illuminate\Support\Facades\Route;


Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->middleware('auth')->name('home');

Route::prefix('admin')
    ->as('admin.')
    ->middleware(['auth', 'account:admin'])
    ->group(function () {

        Route::get('/', [App\Http\Controllers\Admin\MainController::class, 'index'])->name('home');

        Route::post('/users/{user}/deactivate', [App\Http\Controllers\Admin\UserController::class, 'deactivate'])->name('user.deactivate');
        Route::post('/users/{user}/activate', [App\Http\Controllers\Admin\UserController::class, 'activate'])->name('user.activate');
        Route::resource('users', App\Http\Controllers\Admin\UserController::class)->only([
            'index', 'show', 'edit', 'update', 'destroy'
        ]);
        Route::resource('books', App\Http\Controllers\Admin\BookController::class)->only([
            'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
        ]);

        Route::resource('booking', App\Http\Controllers\Admin\BookBookingController::class)->only([
            'index'
        ]);
        Route::resource('rentals', App\Http\Controllers\Admin\BookRentalController::class)->only([
            'index', 'edit', 'update', 'destroy'
        ]);

});

Route::prefix('profile')
    ->as('profile.')
    ->middleware(['auth', 'account:user'])
    ->group(function () {

        Route::get('/', [App\Http\Controllers\Profile\ProfileController::class, 'index'])->name('home');

        Route::get('/search', [App\Http\Controllers\Profile\SearchController::class, 'index'])->name('search');

        Route::prefix('my')->as('user.')->group(function () {
            Route::get('/', [App\Http\Controllers\Profile\ProfileController::class, 'show'])->name('show');
            Route::get('/edit', [App\Http\Controllers\Profile\ProfileController::class, 'edit'])->name('edit');
            Route::post('/update', [App\Http\Controllers\Profile\ProfileController::class, 'update'])->name('update');
        });

        Route::prefix('books')->as('books.')->group(function () {
            Route::get('/', [App\Http\Controllers\Profile\BookController::class, 'index'])->name('index');
            Route::get('{book}', [App\Http\Controllers\Profile\BookController::class, 'show'])->name('show');
            Route::post('{book}/comments', [App\Http\Controllers\Profile\CommentController::class, 'store'])->name('comments.store');
            Route::delete('{book}/comments/{comment}', [App\Http\Controllers\Profile\CommentController::class, 'destroy'])->name('comments.destroy');
        });

        Route::prefix('booking')->as('booking.')->group(function () {
            Route::get('/', [App\Http\Controllers\Profile\BookBookingController::class, 'index'])->name('index');
            Route::post('{book}/booking', [App\Http\Controllers\Profile\BookBookingController::class, 'store'])->name('store');
            Route::delete('{book}/booking/{booking}', [App\Http\Controllers\Profile\BookBookingController::class, 'destroy'])->name('destroy');
        });

        Route::prefix('rentals')->as('rentals.')->group(function () {
            Route::get('/', [App\Http\Controllers\Profile\BookRentalController::class, 'index'])->name('index');
            Route::post('{book}/rental', [App\Http\Controllers\Profile\BookRentalController::class, 'store'])->name('store');
        });

        Route::get('/logs', [App\Http\Controllers\Profile\LogsController::class, 'index'])->name('logs');
});





