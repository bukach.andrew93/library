<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\=Book>
 */
class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake()->title(),
            'description' => fake()->realText(200),
            'publishing_house' => 'test',
            'year_publishing' => fake()->year(),
            'isbn' => 'RE1235468121351',
            'image' => 'books/KciipAseJR8kZ0O7batC5drJnUXM2vBOQI6zT4G2.jpg'
        ];
    }
}
