<?php

namespace Database\Seeders;

use App\Models\LogType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LogTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $logType = new LogType();
        $logType->title = 'booking';
        $logType->save();

        $logType = new LogType();
        $logType->title = 'rental';
        $logType->save();
    }
}
