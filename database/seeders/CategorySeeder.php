<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $category = new Category;
        $category->title = 'Детективы';
        $category->status = 'active';
        $category->save();

        $category = new Category;
        $category->title = 'Фантастика';
        $category->status = 'active';
        $category->save();

        $category = new Category;
        $category->title = 'Классика';
        $category->status = 'active';
        $category->save();
    }
}
