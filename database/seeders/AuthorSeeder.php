<?php

namespace Database\Seeders;

use App\Models\Author;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $author = new Author();
        $author->first_name = 'Лев';
        $author->last_name = 'Толстой';
        $author->status = 'active';
        $author->save();

        $author = new Author();
        $author->first_name = 'Антон';
        $author->last_name = 'Чехов';
        $author->status = 'active';
        $author->save();

        $author = new Author();
        $author->first_name = 'Николай';
        $author->last_name = 'Гоголь';
        $author->status = 'active';
        $author->save();

        $author = new Author();
        $author->first_name = 'Михаил';
        $author->last_name = 'Булгаков';
        $author->status = 'active';
        $author->save();
    }
}
