<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('type_id')->unsigned()->comment('Тип записи');
            $table->bigInteger('book_id')->unsigned()->comment('Книга');
            $table->text('massage')->nullable()->comment('Сообщение');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('logs', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('type_id')->references('id')->on('log_types');
            $table->foreign('book_id')->references('id')->on('books');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('logs', function($table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['type_id']);
            $table->dropForeign(['book_id']);
        });
        Schema::dropIfExists('logs');
    }
};
