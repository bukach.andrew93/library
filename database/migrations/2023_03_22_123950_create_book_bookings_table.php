<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('book_bookings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('book_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->timestamp('booking_to')->comment('Бронировать на');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('book_bookings', function (Blueprint $table) {
            $table->foreign('book_id')->references('id')->on('books');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('book_bookings', function($table) {
            $table->dropForeign(['book_id']);
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('book_bookings');
    }
};
