<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('title')->comment('Название');
            $table->text('description')->nullable()->comment('Описание');
            $table->string('publishing_house')->comment('Издательство');
            $table->year('year_publishing')->comment('Год издания');
            $table->string('isbn')->comment('ISBN');
            $table->string('image')->nullable()->comment('Обложка');
            $table->enum('status', ['active', 'deleted'])->default('active')->comment('Статус');
            $table->timestamps();
            $table->softDeletes();
        });
    }
    // такую как автор, название, издательство, год издания, ISBN и обложку.

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('books');
    }
};
