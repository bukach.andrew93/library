@extends('layouts.app')

@section('content')

    <div class="container">

        @include('admin.components.menu')

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Название книги</th>
                    <th scope="col">ISBN книги</th>
                    <th scope="col">Пользователь</th>
                </tr>
            </thead>
            <tbody>
                @forelse($booking as $book)
                    <tr>
                        <td>{{ $book->book->title }}</td>
                        <td>{{ $book->book->isbn }}</td>
                        <td>#{{ $book->user->id }} {{ $book->user->name }} {{ $book->user->email }}</td>
                    </tr>
                @empty
                    <p>Пусто</p>
                @endforelse
            </tbody>
        </table>

    </div>

@endsection
