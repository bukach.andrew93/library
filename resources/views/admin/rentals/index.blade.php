@extends('layouts.app')

@section('content')

    <div class="container">

        @include('admin.components.menu')

        <table class="table">
            <thead>
            <tr>
                <th scope="col">Название книги</th>
                <th scope="col">ISBN книги</th>
                <th scope="col">Пользователь</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($rentals as $rental)
                <tr>
                    <td>{{ $rental->book->title }}</td>
                    <td>{{ $rental->book->isbn }}</td>
                    <td>#{{ $rental->user->id }} {{ $rental->user->name }} {{ $rental->user->email }}</td>
                    <td>
                        <a href="{{ route('admin.rentals.edit', $rental->id) }}" class="btn btn-primary">Редактирование</a>
                        <form action="{{ route('admin.rentals.destroy', $rental->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Удалить</button>
                        </form>
                    </td>
                </tr>
            @empty
                <p>Пусто</p>
            @endforelse
            </tbody>
        </table>

    </div>

@endsection
