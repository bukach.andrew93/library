@extends('layouts.app')

@section('content')

    <div class="container">

        @include('admin.components.menu')


        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Аренда</div>

                    <div class="card-body">

                        @if($rental)
                            <strong>Книга арендована до {{ date('d-m-Y', strtotime($rental->return_at)) }}</strong>
                        @endif
                        <form method="POST" action="{{ route('admin.rentals.update', $rental) }}">
                            @csrf
                            @method('PUT')

                            <label for="return_at">Дата возврата</label>
                            <div class="row">
                                <div class="col-md-8">
                                    <input type="date" class="form-control @error('return_at') is-invalid @enderror" id="return_at" name="return_at" placeholder="" value="{{ date('d-m-Y', strtotime($rental->return_at)) }}" required="">
                                    @error('return_at')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Изменить
                                    </button>
                                </div>
                            </div>
                        </form>

                        <hr>
                        <br>

                        <form method="POST" action="{{ route('admin.user.deactivate', $rental->user) }}">
                            @csrf

                            <div class="col-md-12">
                                <button type="submit" class="btn btn-warning">
                                    Заблокировать пользователя
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <br>
        <br>

    </div>

@endsection
