@extends('layouts.app')

@section('content')

    <div class="container">

        @include('admin.components.menu')

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Профиль</div>

                    <div class="card-body">

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Роль</label>

                            <div class="col-md-6">
                                <input id="name" type="text" disabled class="form-control" name="role" value="{{ $user->role->name }}" required autocomplete="name" autofocus>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="first_name" class="col-md-4 col-form-label text-md-end">{{ __('FirstName') }}</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" disabled class="form-control" name="first_name" value="{{ $user->first_name }}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="last_name" class="col-md-4 col-form-label text-md-end">{{ __('LastName') }}</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" disabled class="form-control" name="last_name" value="{{ $user->last_name }}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="middle_name" class="col-md-4 col-form-label text-md-end">{{ __('MiddleName') }}</label>

                            <div class="col-md-6">
                                <input id="middle_name" type="text" disabled class="form-control" name="middle_name" value="{{ $user->middle_name }}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Email') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" disabled class="form-control" name="email" value="{{ $user->email }}" required autocomplete="name" autofocus>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <ul class="list-group mb-4 mt-4">
            @forelse($user->logs as $log)
                <li class="list-group-item">
                    <strong>{{ $log->massage ?? '' }}</strong><br>
                    <div>
                        Книга #{{ $log->book->id ?? '' }}<br>
                        {{ $log->book->title ?? '' }}<br>
                        {{ $log->book->isbn ?? '' }}
                    </div>
                </li>
            @empty
                <li class="list-group-item">Нет событий</li>
            @endforelse
        </ul>

    </div>

@endsection
