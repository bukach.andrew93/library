<div class="nav-scroller py-1 mb-2">
    <nav class="nav d-flex">
        <a class="p-2 text-muted" href="{{ route('admin.users.index') }}">Список пользователей</a>
        <a class="p-2 text-muted" href="{{ route('admin.books.index') }}">Список книг</a>
        <a class="p-2 text-muted" href="{{ route('admin.booking.index') }}">Список бронирований</a>
        <a class="p-2 text-muted" href="{{ route('admin.rentals.index') }}">Список аренды книг</a>
    </nav>
</div>
