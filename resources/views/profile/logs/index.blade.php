@extends('layouts.app')

@section('content')

    <div class="container">

        <ul class="list-group mb-4 mt-4">
            @forelse($logs as $log)
                <li class="list-group-item">
                    <strong>{{ $log->massage ?? '' }}</strong><br>
                    <div>
                        Книга #{{ $log->book->id ?? '' }}<br>
                        {{ $log->book->title ?? '' }}<br>
                        {{ $log->book->isbn ?? '' }}
                    </div>
                </li>
            @empty
                <li class="list-group-item">Нет событий</li>
            @endforelse
        </ul>


    </div>

@endsection
