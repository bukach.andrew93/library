@extends('layouts.app')

@section('content')

    <div class="container">

        <h2>Список забронированных книг</h2>

        <div class="row mb-2">

            @foreach($booksBooking as $booking)
                <div class="col-md-6">
                    @include('profile.components.book', ['book' => $booking->book])
                    @if($booking)
                        <hr>
                        <strong>Книга забронирована до {{ date('d-m-Y', strtotime($booking->booking_to)) }}</strong>
                        @if ($booking->user_id === auth()->user()->id)
                            <form action="{{ route('profile.booking.destroy', [$booking->book, $booking]) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm" type="submit">Отменить бронь</button>
                            </form>
                        @endif
                        <br>
                        <br>
                    @endif
                </div>
            @endforeach

        </div>
    </div>

@endsection
