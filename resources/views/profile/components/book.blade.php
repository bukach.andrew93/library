
    <div
        class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
            <strong class="d-inline-block mb-2 text-primary">
                @foreach($book->categories as $category)
                    <span class="badge badge-primary">{{ $category->title }}</span>
                @endforeach
            </strong>
            <strong class="d-inline-block mb-2 text-primary">
                @foreach($book->authors as $author)
                    <span class="badge badge-primary">{{ $author->full_name }}</span>
                @endforeach
            </strong>
            <h3 class="mb-0">{{ $book->title }}</h3>
            <div class="mb-1 text-muted">year: {{ $book->year_publishing }}</div>
            <div class="mb-1 text-muted">isbn: {{ $book->isbn }}</div>
            <a href="{{ route('profile.books.show', $book) }}" class="stretched-link">Подробнее</a>
        </div>
        <div class="col-auto d-none d-lg-block">
            <img src="/storage/{{ $book->image }}" class="bd-placeholder-img" width="200" height="250">
        </div>
    </div>
