@extends('layouts.app')

@section('content')

    @if(session('error'))
        <div class="container mb-4">
            <div class="alert alert-warning">{{ session('error') }}</div>
        </div>
    @endif
    <div class="container">

        <div class="row mb-2">

            <div class="col-md-6">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-primary">
                            Категории:
                            @foreach($book->categories as $category)
                                <span class="badge badge-primary">{{ $category->title }}</span>
                            @endforeach
                        </strong>
                        <strong class="d-inline-block mb-2 text-primary">
                            Авторы:
                            @foreach($book->authors as $author)
                                <span class="badge badge-primary">{{ $author->full_name }}</span>
                            @endforeach
                        </strong>
                        <h3 class="mb-0">{{ $book->title }}</h3>
                        <div class="mb-1 text-muted">year: {{ $book->year_publishing }}</div>
                        <div class="mb-1 text-muted">isbn: {{ $book->isbn }}</div>
                        <div>
                            {{ $book->description }}
                        </div>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <img src="/storage/{{ $book->image }}" class="bd-placeholder-img" width="200" height="250">
                    </div>
                </div>

            </div>
            <div class="col-md-6">

                <div class="mb-4">
                    <h4>Бронирование</h4>

                    @if($book->booking)
                        <strong>Книга забронирована до {{ date('d-m-Y', strtotime($book->booking->booking_to)) }}</strong>
                        @if ($book->booking->user_id === auth()->user()->id)
                            <form action="{{ route('profile.booking.destroy', [$book, $book->booking]) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm" type="submit">Отменить бронь</button>
                            </form>
                        @endif
                    @else
                        <form method="POST" action="{{ route('profile.booking.store', $book) }}">
                            @csrf
                            <label for="booking_to">Бронировать на</label>
                            <div class="row">
                                <div class="col-md-8">
                                    <input type="date" class="form-control @error('booking_to') is-invalid @enderror" id="booking_to" name="booking_to" placeholder="" value="" required="">
                                    @error('booking_to')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Бронировать
                                    </button>
                                </div>
                            </div>
                        </form>
                    @endif
                </div>

                <hr>

                <div>
                    <h4>Аренда</h4>

                    @if($book->rental)
                        <strong>Книга арендована до {{ date('d-m-Y', strtotime($book->rental->return_at)) }}</strong>
                    @else
                        <form method="POST" action="{{ route('profile.rentals.store', $book) }}">
                            @csrf
                            <label for="return_at">Дата возврата</label>
                            <div class="row">
                                <div class="col-md-8">
                                    <input type="date" class="form-control @error('return_at') is-invalid @enderror" id="return_at" name="return_at" placeholder="" value="" required="">
                                    @error('return_at')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Арендовать
                                    </button>
                                </div>
                            </div>
                        </form>
                    @endif

                </div>


            </div>

        </div>

        <hr>

        <div class="row mb-4">
            <div class="col-lg-8">

                @forelse($book->comments as $comment)
                    <div class="alert alert-secondary" role="alert">

                        <div class="rating">
                            <i class="bi bi-star  @if ($comment->rating >= 1) rating-active @endif" style="font-size: 1rem;"></i>
                            <i class="bi bi-star  @if ($comment->rating >= 2) rating-active @endif" style="font-size: 1rem;"></i>
                            <i class="bi bi-star  @if ($comment->rating >= 3) rating-active @endif" style="font-size: 1rem;"></i>
                            <i class="bi bi-star  @if ($comment->rating >= 4) rating-active @endif" style="font-size: 1rem;"></i>
                            <i class="bi bi-star  @if ($comment->rating >= 5) rating-active @endif" style="font-size: 1rem;"></i>
                        </div>
                        <h4>{{ $comment->title }}</h4>
                        <p>{{ $comment->text }}</p>

                        @if ($comment->user_id === auth()->user()->id)
                            <form action="{{ route('profile.books.comments.destroy', [$book, $comment]) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm" type="submit">Удалить</button>
                            </form>
                        @endif
                    </div>
                @empty
                    <center>Комментариев нет</center>
                @endforelse

                <form method="POST" action="{{ route('profile.books.comments.store', $book) }}">
                    @csrf
                    <div class="row">

                        <div class="col-md-12 mb-3">
                            <label for="title">Рейтинг</label>
                            <div class="rating">
                                <input type="radio" id="rating_1" name="rating" value="1" style="display: none">
                                <input type="radio" id="rating_2" name="rating" value="2" style="display: none">
                                <input type="radio" id="rating_3" name="rating" value="3" style="display: none">
                                <input type="radio" id="rating_4" name="rating" value="4" style="display: none">
                                <input type="radio" id="rating_5" name="rating" value="5" style="display: none">
                                <div class="stars @error('rating') is-invalid @enderror">
                                    <label for="rating_1">
                                        <i class="bi bi-star" style="font-size: 1rem;"></i>
                                    </label>
                                    <label for="rating_2">
                                        <i class="bi bi-star" style="font-size: 1rem;"></i>
                                    </label>
                                    <label for="rating_3">
                                        <i class="bi bi-star" style="font-size: 1rem;"></i>
                                    </label>
                                    <label for="rating_4">
                                        <i class="bi bi-star" style="font-size: 1rem;"></i>
                                    </label>
                                    <label for="rating_5">
                                        <i class="bi bi-star" style="font-size: 1rem;"></i>
                                    </label>
                                </div>
                                @error('rating')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="title">Заголовок</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="" value="" required="">
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="text">Текст</label>
                            <textarea type="text" class="form-control @error('text') is-invalid @enderror" id="text" name="text" placeholder="" required=""></textarea>
                            @error('text')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                    </div>
                    <div class="row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Оставить комментарий
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection
