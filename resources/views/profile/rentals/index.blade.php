@extends('layouts.app')

@section('content')

    <div class="container">

        <h2>Список забронированных книг</h2>

        <div class="row mb-2">

            @foreach($booksRental as $rental)
                <div class="col-md-6">
                    @include('profile.components.book', ['book' => $rental->book])
                    @if($rental)
                        <hr>
                        <strong>Книга арендована до {{ date('d-m-Y', strtotime($rental->booking_to)) }}</strong>
                        <br>
                        <br>
                    @endif
                </div>
            @endforeach

        </div>
    </div>

@endsection
