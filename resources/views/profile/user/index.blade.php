@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="nav-scroller py-1 mb-2">
            <nav class="nav d-flex">
                <a class="p-2 text-muted" href="{{ route('profile.user.show') }}">Профиль</a>
                <a class="p-2 text-muted" href="{{ route('profile.booking.index') }}">Забронированные книги</a>
                <a class="p-2 text-muted" href="{{ route('profile.rentals.index') }}">Арендованные книги</a>
                <a class="p-2 text-muted" href="{{ route('profile.logs') }}">Список событий</a>
            </nav>
        </div>


    </div>

@endsection
