@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="nav-scroller py-1 mb-2">
            <nav class="nav d-flex justify-content-between">
                <a class="p-2 text-muted" href="{{ route('profile.user.edit') }}">Изменить профиль</a>
            </nav>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Профиль</div>

                    <div class="card-body">
                        <div class="row mb-3">
                            <label for="first_name" class="col-md-4 col-form-label text-md-end">{{ __('FirstName') }}</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" disabled class="form-control" name="first_name" value="{{ $user->first_name }}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="last_name" class="col-md-4 col-form-label text-md-end">{{ __('LastName') }}</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" disabled class="form-control" name="last_name" value="{{ $user->last_name }}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="middle_name" class="col-md-4 col-form-label text-md-end">{{ __('MiddleName') }}</label>

                            <div class="col-md-6">
                                <input id="middle_name" type="text" disabled class="form-control" name="middle_name" value="{{ $user->middle_name }}">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="text" disabled class="form-control" name="email" value="{{ $user->email }}">
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection
