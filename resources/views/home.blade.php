@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
            <div class="col-md-6 px-0">
                <h1 class="display-4 font-italic">Title of a longer featured blog post</h1>
                <p class="lead my-3">Multiple lines of text that form the lede, informing new readers quickly and
                    efficiently about what’s most interesting in this post’s contents.</p>
                <p class="lead mb-0"><a href="#" class="text-white font-weight-bold">Continue reading...</a></p>
            </div>
        </div>

        <form method="GET" action="{{ route('home') }}">
            <nav class="blog-pagination mb-4">
                <button class="btn btn-outline-secondary" type="submit" name="title" value="ASC">По Названию</button>
                <button class="btn btn-outline-secondary" type="submit" name="year" value="ASC">По Году</button>
                <button class="btn btn-outline-secondary" type="submit" name="author" value="ASC">По Автору</button>
                <button class="btn btn-outline-secondary" type="submit" name="category" value="ASC">По Жанру</button>
            </nav>
        </form>

        <div class="row mb-2">

            @forelse($books as $book)
                <div class="col-md-6">
                    @include('profile.components.book', ['book' => $book])
                </div>
            @empty
                <p>Пусто</p>
            @endforelse

        </div>
    </div>

@endsection
