<?php

namespace App\Repositories;

use Illuminate\Http\Request;

interface Repository
{
    public function find($id);
    public function paginate(Request $request);
    public function all();
}
