<?php

namespace App\Repositories;

use App\Models\Book;
use Illuminate\Http\Request;

class BookRepository implements Repository
{
    public function find($id)
    {
        return Book::find($id);
    }

    public function paginate(Request $request)
    {
        $books = Book::query();
        $books->where('books.status', 'active');


        $books->with('categories')
            ->with('authors');

        if ($request->filled('title')) {
            $books->orderBy('title', $request->input('title'));
        }

        if ($request->filled('year')) {
            $books->orderBy('year_publishing', $request->input('year'));
        }

        if ($request->filled('author')) {
            $books->selectRaw('books.*, authors.first_name as authors_name')
                ->join('author_book', 'books.id', '=', 'author_book.book_id')
                ->join('authors', 'author_book.author_id', '=', 'authors.id')
                ->orderBy('authors.first_name', $request->input('author'));
        }

        if ($request->filled('category')) {
            $books->selectRaw('books.*, categories.title as categories_name')
                ->join('book_category', 'books.id', '=', 'book_category.book_id')
                ->join('categories', 'book_category.category_id', '=', 'categories.id')
                ->orderBy('categories.title', $request->input('category'));
        }

        $books = $books->paginate();

        return $books;
    }

    public function all()
    {
        return Book::all();
    }
}
