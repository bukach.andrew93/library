<?php

namespace App\Services;

use App\Models\Book;
use App\Models\Log;
use App\Models\LogType;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class LogService
{
    public static function create(User $user, LogType $type, Book $book, string $massage)
    {
        return DB::transaction(function () use ($user, $type, $book, $massage) {
            return Log::create([
                'user_id' => $user->id,
                'type_id' => $type->id,
                'book_id' => $book->id,
                'massage' => $massage
            ]);
        });
    }
}
