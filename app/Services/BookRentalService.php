<?php

namespace App\Services;

use App\Http\Requests\Admin\BookRentalUpdateRequest;
use App\Http\Requests\Profile\BookRentalRequest;
use App\Models\Book;
use App\Models\BookRental;
use App\Models\LogType;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class BookRentalService
{
    public function create(BookRentalRequest $request, Book $book, User $user)
    {
        return DB::transaction(function () use ($request, $book, $user) {
            $data = [
                'return_at' => $request->date('return_at'),
                'book_id' => $book->id,
                'user_id' => $user->id,
            ];
            $rental = BookRental::create($data);
            LogService::create($rental->user, LogType::find(2), $book, 'Аренда создана');
            return $rental;
        });
    }

    public function remove(BookRental $rental)
    {
        return DB::transaction(function () use ($rental) {
            $rental->delete();
            LogService::create($rental->user, LogType::find(2), $rental->book, 'Аренда удалена');
        });
    }

    public function update(BookRentalUpdateRequest $request, BookRental $rental)
    {
        return DB::transaction(function () use ($request, $rental) {
            return $rental->update([
                'return_at' => $request->date('return_at'),
            ]);
        });
    }
}
