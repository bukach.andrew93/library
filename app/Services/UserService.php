<?php
declare(strict_types=1);

namespace App\Services;

use App\Http\Requests\Admin\UserUpdateRequest;
use App\Http\Requests\Profile\ProfileRequest;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public function update(UserUpdateRequest $request, User $user)
    {
        return DB::transaction(function () use ($request, $user) {
            $user->update([
                'role_id' => $request->role,
                'name' => $request->name,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'middle_name' => $request->middle_name,
                'email' => $request->email,
            ]);
        });
    }

    public function updateProfile(ProfileRequest $request, User $user)
    {
        return DB::transaction(function () use ($request, $user) {
            $user->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'middle_name' => $request->middle_name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
        });
    }

    public function remove(User $user)
    {
        return DB::transaction(function () use ($user) {
            $user->delete();
        });
    }

    public function activate(User $user)
    {
        return DB::transaction(function () use ($user) {
            $user->update([
                'status' => 'active'
            ]);
        });
    }

    public function deactivate(User $user)
    {
        return DB::transaction(function () use ($user) {
            $user->update([
                'status' => 'deactivated'
            ]);
        });
    }
}
