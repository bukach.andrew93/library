<?php

namespace App\Services;

use App\Http\Requests\Profile\BookBookingRequest;
use App\Models\Book;
use App\Models\BookBooking;
use App\Models\LogType;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class BookBookingService
{

    public function create(BookBookingRequest $request, Book $book, User $user)
    {
        return DB::transaction(function () use ($request, $book, $user) {
            $data = [
                'booking_to' => $request->date('booking_to'),
                'book_id' => $book->id,
                'user_id' => $user->id,
            ];
            $booking = BookBooking::create($data);
            LogService::create($user, LogType::find(1), $book, 'Бронирование создано');
            return $booking;
        });
    }

    public function remove(BookBooking $booking, Book $book, User $user)
    {
        return DB::transaction(function () use ($booking, $user, $book) {
            $booking->delete();
            LogService::create($user, LogType::find(1), $book, 'Бронирование удаленно');
        });
    }
}
