<?php
declare(strict_types=1);

namespace App\Services;

use App\Http\Requests\Admin\BookStoreRequest;
use App\Http\Requests\Admin\BookUpdateRequest;
use App\Http\Requests\Profile\CommentDestroyRequest;
use App\Http\Requests\Profile\CommentStoreRequest;
use App\Models\Book;
use App\Models\Comment;
use Illuminate\Support\Facades\DB;

class BookService
{
    public function create(BookStoreRequest $request): Book
    {
        return DB::transaction(function () use ($request) {
            $path = $request->file('image')->store('books', 'public') ?? null;

            $book = Book::create([
                'title' => $request->title,
                'description' => $request->description ?? null,
                'publishing_house' => $request->publishing_house,
                'year_publishing' => $request->year_publishing,
                'isbn' => $request->isbn,
                'image' => $path,
            ]);

            if ($request->filled('categories')) {
                $book->categories()->attach($request->categories);
            }

            if ($request->filled('authors')) {
                $book->authors()->attach($request->authors);
            }

            return $book;
        });
    }

    public function update(BookUpdateRequest $request, Book $book): Book
    {
        return DB::transaction(function () use ($request, $book) {
            $path = $request->file('image')->store('books', 'public') ?? $book->image;

            $book->update([
                'title' => $request->title,
                'description' => $request->description ?? null,
                'publishing_house' => $request->publishing_house,
                'year_publishing' => $request->year_publishing,
                'isbn' => $request->isbn,
                'image' => $path ?? null,
            ]);

            if ($request->filled('categories')) {
                $book->categories()->sync($request->categories);
            }

            if ($request->filled('authors')) {
                $book->authors()->sync($request->authors);
            }

            return $book;
        });
    }

    public function remove(Book $book)
    {
        return DB::transaction(function () use ($book) {
            $book->categories()->detach();
            return $book->delete();
        });
    }

    public function addComment(CommentStoreRequest $request, Book $book)
    {
        return DB::transaction(function () use ($request, $book) {
            return $book->comments()->create([
                'title' => $request->title,
                'text'  => $request->text,
                'rating'  => $request->rating,
                'book_id'  => $book->id,
                'user_id'  => auth()->id(),
            ]);
        });
    }

    public function removeComment(Comment $comment)
    {
        return DB::transaction(function () use ($comment) {
            return $comment->delete();
        });
    }
}
