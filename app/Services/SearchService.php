<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Book;
use Illuminate\Http\Request;

class SearchService
{
    public function search(Request $request)
    {
        $querySearch = trim($request->input('search'));
        $books = Book::where('status', 'active')
                ->where(function ($query) use ($querySearch) {
                    $query->where('title', 'like', '%'.$querySearch.'%')
                        ->orWhere('description', 'like', '%'.$querySearch.'%')
                        ->orWhere('isbn', 'like', '%'.$querySearch.'%')
                        ->orWhere('year_publishing', 'like', '%'.$querySearch.'%')
                        ->orWhereHas('categories', function ($queryCategory) use ($querySearch) {
                            $queryCategory->where('title', 'like', '%'.$querySearch.'%');
                        })
                        ->orWhereHas('authors', function ($queryAuthor) use ($querySearch) {
                            $queryAuthor->where('first_name', 'like', '%'.$querySearch.'%')
                                ->orWhere('last_name', 'like', '%'.$querySearch.'%')
                                ->orWhere('middle_name', 'like', '%'.$querySearch.'%');
                        });
                })
                ->with('categories')
                ->get();

        return $books;
    }
}
