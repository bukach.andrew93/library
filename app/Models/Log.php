<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Log extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'logs';

    protected $fillable = [
        'user_id',
        'type_id',
        'book_id',
        'massage',
    ];

    public function user()
    {
        return $this->hasOne(User::class,'id', 'user_id');
    }

    public function book()
    {
        return $this->hasOne(Book::class,'id', 'book_id');
    }

    public function logType()
    {
        return $this->hasOne(LogType::class,'id', 'type_id');
    }
}
