<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'books';

    protected $fillable = [
        'title',
        'description',
        'publishing_house',
        'year_publishing',
        'isbn',
        'image'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function authors()
    {
        return $this->belongsToMany(Author::class)->withPivot('author_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,'book_id', 'id');
    }

    public function booking()
    {
        return $this->hasOne(BookBooking::class,'book_id', 'id');
    }

    public function rental()
    {
        return $this->hasOne(BookRental::class,'book_id', 'id');
    }
}
