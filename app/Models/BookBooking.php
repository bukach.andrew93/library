<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookBooking extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'book_bookings';

    protected $fillable = [
        'book_id',
        'user_id',
        'booking_to',
    ];

    public function book()
    {
        return $this->hasOne(Book::class,'id', 'book_id');
    }

    public function user()
    {
        return $this->hasOne(User::class,'id', 'user_id');
    }
}
