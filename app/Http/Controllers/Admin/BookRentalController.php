<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BookRentalUpdateRequest;
use App\Models\BookRental;
use App\Services\BookRentalService;
use Illuminate\Http\Request;

class BookRentalController extends Controller
{
    public function index()
    {
        $rentals = BookRental::with('book')->with('user')->get();
        return view('admin.rentals.index', ['rentals' => $rentals]);
    }

    public function edit(BookRental $rental)
    {
        return view('admin.rentals.edit', ['rental' => $rental]);
    }

    public function update(BookRentalUpdateRequest $request, BookRental $rental, BookRentalService $bookRentalService)
    {
        $bookRentalService->update($request, $rental);
        return redirect()->route('admin.rentals.edit', $rental);
    }

    public function destroy(BookRental $rental, BookRentalService $bookRentalService)
    {
        $bookRentalService->remove($rental);
        return redirect()->route('admin.rentals.index');
    }
}
