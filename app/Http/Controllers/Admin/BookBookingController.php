<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BookBooking;
use App\Services\BookBookingService;
use Illuminate\Http\Request;

class BookBookingController extends Controller
{
    public function index()
    {
        $booking = BookBooking::with('book')->with('user')->get();
        return view('admin.booking.index', ['booking' => $booking]);
    }
}
