<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BookStoreRequest;
use App\Http\Requests\Admin\BookUpdateRequest;
use App\Models\Author;
use App\Models\Book;
use App\Models\Category;
use App\Services\BookService;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index()
    {
        $books = Book::get();
        return view('admin.books.index', ['books' => $books]);
    }

    public function create(Request $request)
    {
        $categories = Category::get();
        $authors = Author::get();
        return view('admin.books.create', ['categories' => $categories, 'authors' => $authors]);
    }

    public function store(BookStoreRequest $request, BookService $bookService)
    {
        $book = $bookService->create($request);

        return redirect()->route('admin.books.show', $book);
    }

    public function show(Book $book)
    {
        return view('admin.books.show', ['book' => $book]);
    }

    public function edit(Book $book)
    {
        $categories = Category::get();
        $authors = Author::get();
        return view('admin.books.edit', ['book' => $book, 'categories' => $categories, 'authors' => $authors]);
    }

    public function update(BookUpdateRequest $request, Book $book, BookService $bookService)
    {
        $bookService->update($request, $book);

        return redirect()->route('admin.books.show', $book);
    }

    public function destroy(Book $book, BookService $bookService)
    {
        $bookService->remove($book);

        return redirect()->route('admin.books.index');
    }
}
