<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserUpdateRequest;
use App\Models\Role;
use App\Models\User;
use App\Services\UserService;

class UserController extends Controller
{
    public function index()
    {
        $users = User::with('role')->get();
        return view('admin.users.index', ['users' => $users]);
    }

    public function show(User $user)
    {
        return view('admin.users.show', ['user' => $user]);
    }

    public function edit(User $user)
    {
        $roles = Role::get();
        return view('admin.users.edit', ['user' => $user, 'roles' => $roles]);
    }

    public function update(UserUpdateRequest $request, User $user, UserService $userService)
    {
        $userService->update($request, $user);

        return redirect()->route('admin.users.show', $user);
    }

    public function destroy(User $user, UserService $userService)
    {
        $userService->remove($user);

        return redirect()->route('admin.users.index');
    }

    public function activate(User $user, UserService $userService)
    {
        $userService->activate($user);
        return redirect()->route('admin.users.edit', $user);
    }

    public function deactivate(User $user, UserService $userService)
    {
        $userService->deactivate($user);
        return redirect()->route('admin.users.edit', $user);
    }
}
