<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Models\Log;
use Illuminate\Http\Request;

class LogsController extends Controller
{
    public function index()
    {
        $logs = Log::where('user_id', auth()->id())->with('book')->with('logType')->get();
        return view('profile.logs.index', ['logs' => $logs]);
    }
}
