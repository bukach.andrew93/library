<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\CommentStoreRequest;
use App\Models\Book;
use App\Models\Comment;
use App\Services\BookService;

class CommentController extends Controller
{
    public function store(CommentStoreRequest $request, Book $book, BookService $bookService)
    {
        $bookService->addComment($request, $book);
        return redirect()->route('profile.books.show', $book);
    }

    public function destroy(Book $book, Comment $comment, BookService $bookService)
    {
        if ($comment->user_id !== auth()->id()) {
            return redirect()->route('profile.books.show', $book)->with('error', 'Пользователь не оставлял этот комментарий');
        }

        $bookService->removeComment($comment);
        return redirect()->route('profile.books.show', $book);
    }
}
