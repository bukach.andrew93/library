<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\BookRentalRequest;
use App\Models\Book;
use App\Models\BookRental;
use App\Services\BookRentalService;

class BookRentalController extends Controller
{
    public function index()
    {
        $booksRental = BookRental::where('user_id', auth()->id())
            ->with('book', function ($book) {
                $book->with('categories')
                    ->with('authors');
            })
            ->get();
        return view('profile.rentals.index', ['booksRental' => $booksRental]);
    }

    public function store(BookRentalRequest $request, Book $book, BookRentalService $rentalService)
    {
        if ($book->rental) {
            return redirect()->route('profile.books.show', $book)->with('error', 'Книга Арендована');
        }

        if (auth()->user()->status === 'deactivated') {
            return redirect()->route('profile.books.show', $book)->with('error', 'Пользователь заблокирован');
        }

        $rentalService->create($request, $book, auth()->user());
        return redirect()->route('profile.books.show', $book);
    }
}
