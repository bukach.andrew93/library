<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\ProfileRequest;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        return view('profile.user.index');
    }

    public function show()
    {
        $user = Auth::user();

        return view('profile.user.show', ['user' => $user]);
    }

    public function edit()
    {
        $user = Auth::user();

        return view('profile.user.edit', ['user' => $user]);
    }

    public function update(ProfileRequest $request, UserService $userService)
    {
        $user = Auth::user();

        $userService->updateProfile($request, $user);

        return redirect()->route('profile.user.edit');
    }
}
