<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\BookBookingRequest;
use App\Models\Book;
use App\Models\BookBooking;
use App\Services\BookBookingService;

class BookBookingController extends Controller
{
    public function index()
    {
        $booksBooking = BookBooking::where('user_id', auth()->id())
            ->with('book', function ($book) {
                $book->with('categories')
                ->with('authors');
            })
            ->get();
        return view('profile.booking.index', ['booksBooking' => $booksBooking]);
    }

    public function store(BookBookingRequest $request, Book $book, BookBookingService $bookingService)
    {
        if ($book->booking) {
            return redirect()->route('profile.books.show', $book)->with('error', 'Книга забронирована');
        }

        if (auth()->user()->status === 'deactivated') {
            return redirect()->route('profile.books.show', $book)->with('error', 'Пользователь заблокирован');
        }

        $bookingService->create($request, $book, auth()->user());
        return redirect()->route('profile.books.show', $book);
    }

    public function destroy(Book $book, BookBooking $booking, BookBookingService $bookingService)
    {
        if ($booking->user_id !== auth()->id() || $booking->book_id !== $book->id) {
            return redirect()->route('profile.books.show', $book)->with('error', 'Пользователь не арендовал эту книгу');
        }

        $bookingService->remove($booking, $book, auth()->user());
        return redirect()->route('profile.books.show', $book);
    }
}
