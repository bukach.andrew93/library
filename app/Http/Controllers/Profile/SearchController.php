<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Services\SearchService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request, SearchService $search)
    {
        if (!$request->input('search') || mb_strlen($request->input('search')) <= 2) {
            return view('profile.search.index', ['books' => []]);
        }
        $books = $search->search($request);
        return view('profile.search.index', ['books' => $books]);
    }
}
