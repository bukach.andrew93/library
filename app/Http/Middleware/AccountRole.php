<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;

class AccountRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, ...$guards): Response
    {
        $guards = empty($guards) ? [null] : $guards;
        $user = Auth::user()->role()->first();

        $valueCheck = false;
        foreach ($guards as $guard) {
            if ($guard === $user->name) {
                $valueCheck = true;
            }
        }

        if (!$valueCheck) {
            return redirect(RouteServiceProvider::HOME);
        }

        return $next($request);
    }
}
